from django.db import models
# Create your models here.


class Tema(models.Model):
    nombre = models.CharField(max_length=30)
    icono = models.ImageField()
